-- phpMyAdmin SQL Dump
-- version 5.0.0
-- https://www.phpmyadmin.net/
--
-- Host: discover.bde.inp
-- Generation Time: Sep 30, 2020 at 02:21 PM
-- Server version: 10.3.23-MariaDB-0+deb10u1
-- PHP Version: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boochat`
--

-- --------------------------------------------------------

--
-- Table structure for table `boochat_users`
--

CREATE TABLE `boochat_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `inp_id` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(3) UNSIGNED NOT NULL COMMENT '0 (banned) - 255 (admin)',
  `can_post_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `boochat_users`
--

INSERT INTO `boochat_users` (`id`, `team_id`, `inp_id`, `nickname`, `level`, `can_post_at`) VALUES
(1, 1, 'benaimg', 'Gautier', 255, '2020-02-28 15:04:47'),
(51, 1, 'thobys', 'nigthmared', 100, '2020-03-02 19:44:06'),
(58, 1, 'sabrass', 'link', 50, '2020-03-02 20:02:12'),
(59, 1, 'duvignt', 'Akiro', 50, '2020-03-02 20:07:17'),
(61, 1, 'pillott', 'Ozon', 100, '2020-03-02 19:57:20'),
(62, 1, 'mattera', 'raubin', 1, '2020-03-03 21:12:24'),
(68, 2, 'petitth', 'T.O.', 1, '2020-03-03 17:13:16'),
(69, 1, 'malmair', 'Kern', 100, '2020-03-03 16:09:36'),
(70, 2, 'figuinc', 'Figuax', 1, '2020-03-03 16:45:42'),
(73, 2, 'giletc', 'Hors-Contexte', 1, '2020-03-03 16:39:45'),
(74, 3, 'desombp', 'Sexcretaire', 1, '2020-03-03 19:32:34'),
(75, 2, 'couzinr', 'Soneca', 1, '2020-03-03 19:32:21'),
(76, 1, 'lauverc', 'lauverc', 1, '2020-03-03 18:21:41'),
(81, 3, 'lacailp', 'VP_49-3', 1, '2020-03-03 19:31:14'),
(82, 3, 'constac', 'Colin', 1, '2020-03-03 18:42:35'),
(83, 2, 'ducoina', 'Soundproof', 1, '2020-03-03 18:52:51'),
(85, 2, 'bauda', 'Alex', 1, '2020-03-03 18:47:02'),
(89, 3, 'ceugnaj', '3AKeugnart', 1, '2020-03-03 19:00:16'),
(90, 3, 'massonm', 'RespoBordelSousB00', 1, '2020-03-03 19:32:16'),
(91, 8, 'augers', 'Chaise', 1, '2020-03-03 19:27:14'),
(92, 3, 'bondun', 'JEman', 1, '2020-03-03 19:16:25'),
(93, 6, 'foretm', 'LeGate', 1, '2020-03-03 19:22:26'),
(94, 3, 'badiern', 'VP_light', 1, '2020-03-03 19:31:25'),
(95, 12, 'magnalm', 'LeChef', 1, '2020-03-03 18:57:49'),
(96, 4, 'thedonv', 'Nope.', 1, '2020-03-03 19:21:55'),
(102, 1, 'giraudj', 'kadren', 1, '2020-03-03 20:47:12'),
(103, 1, 'carbona', 'AE_INP', 1, '2020-03-03 19:43:36'),
(105, 5, 'alloucg', 'onmappellelovnix', 1, '2020-03-03 19:07:57'),
(109, 5, 'ferain', 'azix', 1, '2020-03-03 19:05:22'),
(110, 2, 'rivolac', 'EinderJam', 1, '2020-03-03 19:06:13'),
(111, 4, 'jurquee', 'HellYes', 1, '2020-03-03 19:03:59'),
(112, 10, 'sadurnt', 'Tropmathise', 1, '2020-03-03 19:04:40'),
(113, 5, 'davidr', 'Detritus', 1, '2020-03-03 19:08:38'),
(114, 5, 'brettep', 'Tom', 1, '2020-03-03 19:05:09'),
(115, 5, 'thillot', 'Summerbodix', 1, '2020-03-03 19:06:51'),
(116, 10, 'thuetq', 'krotin', 1, '2020-03-03 19:25:26'),
(117, 2, 'puigc', 'Lomfort', 1, '2020-03-03 19:30:30'),
(118, 10, 'foinn', 'Plusviteuuuuh', 1, '2020-03-03 19:28:35'),
(119, 5, 'lezzanc', '5demix', 1, '2020-03-03 19:17:56'),
(120, 4, 'perroub', 'perrob', 0, '2020-03-03 19:13:46'),
(121, 4, 'pacletl', 'loupiotte', 1, '2020-03-03 19:06:45'),
(122, 5, 'migliat', 'Alex_Lab', 1, '2020-03-03 19:20:48'),
(123, 5, 'romanot', 'N7riiiiiix', 1, '2020-03-03 19:20:45'),
(125, 5, 'serresa', 'Jimihendrix', 1, '2020-03-03 19:23:48'),
(126, 2, 'falomiy', 'Prez', 1, '2020-03-03 19:31:39'),
(129, 5, 'caderoy', 'percutemoilefut', 1, '2020-03-03 19:30:09'),
(130, 4, 'carna', 'tocard7', 1, '2020-03-03 19:09:00'),
(132, 4, 'loizeam', 'MAXIME', 1, '2020-03-03 19:33:09'),
(134, 4, 'gutters', 'Seum7', 1, '2020-03-03 19:11:01'),
(135, 2, 'sarlatt', 'Oulah', 1, '2020-03-03 19:15:53'),
(138, 4, 'mennega', 'Ilovegat7bydeco', 1, '2020-03-03 19:12:17'),
(140, 10, 'abdoula', 'Leo7', 1, '2020-03-03 19:12:18'),
(144, 4, 'pierref', 'LeFby', 1, '2020-03-03 19:28:25'),
(145, 4, 'atlans', 'SSS', 1, '2020-03-03 19:13:39'),
(146, 6, 'amathim', 'Marinou', 1, '2020-03-03 19:14:56'),
(149, 9, 'demaraf', 'ClubTechnique', 1, '2020-03-03 19:21:03'),
(150, 9, 'clugerl', 'Ody7AllerAller', 1, '2020-03-03 19:31:18'),
(152, 6, 'labordal', 'Elian_egreto', 1, '2020-03-03 19:17:06'),
(155, 4, 'amicham', 'Mathou', 1, '2020-03-03 19:18:40'),
(156, 6, 'martina3', 'Photo7', 1, '2020-03-03 19:15:14'),
(158, 7, 'ruffiet', 'TeoLeBo', 1, '2020-03-03 19:27:59'),
(159, 6, 'fronorl', 'Trezo', 1, '2020-03-03 19:19:07'),
(160, 7, 'andread', 'Adrien', 1, '2020-03-03 19:16:42'),
(163, 6, 'chambeg', 'Poulet_frites', 1, '2020-03-03 19:30:15'),
(167, 3, 'campion', 'Club_Zik', 1, '2020-03-03 19:29:21'),
(175, 6, 'latrayp', 'Pierrou', 1, '2020-03-03 19:23:49'),
(176, 9, 'grithen', 'Ody7AllerAllee', 1, '2020-03-03 19:31:19'),
(180, 6, 'simonnga', 'GoRooftop', 1, '2020-03-03 19:18:47'),
(183, 6, 'benoisc', 'Respo_Armoire_Elec', 1, '2020-03-03 19:26:14'),
(189, 7, 'biancol', 'Biancorni', 0, '2020-03-03 19:25:43'),
(195, 5, 'carronma', 'Apprentix', 1, '2020-03-03 19:19:26'),
(197, 4, 'nzik', 'RashfordUTD', 1, '2020-03-03 19:28:30'),
(203, 9, 'porchel', 'PtitLoup', 1, '2020-03-03 19:24:04'),
(204, 7, 'grosa', 'Grocho', 1, '2020-03-03 19:26:39'),
(205, 6, 'bouletn', 'Chasseresse', 1, '2020-03-03 19:27:34'),
(206, 6, 'jaumeta', 'Jaumet_la', 1, '2020-03-03 19:23:13'),
(207, 4, 'morataj', 'Jules', 1, '2020-03-03 19:22:13'),
(208, 4, 'scharta', 'Apprentiarrache', 1, '2020-03-03 19:24:02'),
(209, 7, 'akifa', 'Flambeur', 1, '2020-03-03 19:26:06'),
(210, 2, 'alcoufr', 'Respo_B00', 1, '2020-03-03 19:32:19'),
(212, 4, 'pandalm', 'Zoulettedemarcel', 1, '2020-03-03 19:23:27'),
(213, 5, 'fuhrmav', 'Beaufix', 1, '2020-03-03 19:23:58'),
(214, 4, 'narbonv', 'Oss117-san', 1, '2020-03-03 19:31:55'),
(215, 5, 'harol', 'Footix', 1, '2020-03-03 19:32:18'),
(216, 5, 'vidaudj', 'Bruno.Dagues', 1, '2020-03-03 19:32:30'),
(218, 4, 'dembeld', 'Aurel_de_wish', 1, '2020-03-03 19:31:47'),
(220, 5, 'beste', 'Uneranix', 1, '2020-03-03 19:31:09'),
(223, 6, 'fredern', 'Shs', 1, '2020-03-03 19:26:11'),
(225, 9, 'boineta', 'Adam', 1, '2020-03-03 19:42:43'),
(227, 4, 'dedieube', 'Benoit', 1, '2020-03-03 19:26:49'),
(229, 1, 'conzelf', 'Zephyr', 1, '2020-03-03 19:42:49'),
(235, 4, 'marsegl', 'Heeeeeeeeeeeeeeeeiii', 1, '2020-03-03 19:28:22'),
(238, 7, 'graffim', 'Ahbahsuper', 1, '2020-03-03 19:31:49'),
(239, 11, 'willeml', 'Celtista', 1, '2020-03-03 19:30:37'),
(240, 4, 'schmidn', 'Garcia_traitre', 1, '2020-03-03 19:28:29'),
(244, 6, 'maquaij', 'JUJU', 1, '2020-03-03 19:32:25'),
(245, 5, 'launois', 'malvane_leblanc', 1, '2020-03-03 19:32:27'),
(246, 9, 'dargett', 'Slt', 1, '2020-03-03 19:30:49'),
(251, 6, 'bourger', 'Pain', 1, '2020-03-03 19:31:37'),
(255, 2, 'dallavt', 'VPnerve', 1, '2020-03-03 19:32:34'),
(256, 9, 'jamine', '3AConnasse', 1, '2020-03-03 19:32:37'),
(257, 12, 'leonarj', 'MIXITESOCIALE', 1, '2020-03-03 19:32:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boochat_users`
--
ALTER TABLE `boochat_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `inp_id` (`inp_id`),
  ADD UNIQUE KEY `nickname` (`nickname`),
  ADD KEY `team_id` (`team_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boochat_users`
--
ALTER TABLE `boochat_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `boochat_users`
--
ALTER TABLE `boochat_users`
  ADD CONSTRAINT `FK_boochat_users_boochat_teams` FOREIGN KEY (`team_id`) REFERENCES `boochat_teams` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

