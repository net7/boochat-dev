// Permission levels
export const levels = {
  muted: 0,
  chat: 1,
  moderator: 50,
  administrator: 100,
  master: 255
}

// Spam levels
export const spamScores = {
  moderated: 1.0,
  discarded: 2.5
}
