import http from 'http'
import path from 'path'
import express from 'express'
import socketio from 'socket.io'
import mariadb from 'mariadb'
import session from 'express-session'
import SessionRsdbStore from 'express-session-rsdb'
import Conf from 'conf'
import CasAuthentication from 'cas-authentication'
import MysqlApp from './mysql-app'
import expressApp from './express-app'
import socketioApp from './socketio-app'

// `config` is editable and automatically saved at runtime
const serverConfig: Conf = new Conf({
  clearInvalidConfig: false,
  cwd: path.dirname(__dirname),
  configName: 'server-config'
})
const appConfig: Conf = new Conf({
  clearInvalidConfig: false,
  cwd: path.dirname(__dirname),
  configName: 'app-config'
})

// Configure and connect to a MySQL/MariaDB server
const mysqlApp = new MysqlApp(
  mariadb.createPool(serverConfig.get('mariadb'))
)

// Configure the Central Authentication Service (CAS) client
const cas = new CasAuthentication(serverConfig.get('cas'))

// Open a session store (in flat .json files)
const expressSession = session(Object.assign(serverConfig.get('session'), {
  store: new SessionRsdbStore(serverConfig.get('session-rsdb-store'))
}))

// Create the HTTP and WebSocket servers
const app: express.Application = express()
const server: http.Server = new http.Server(app)
const io: socketio.Server = socketio(server)

// Register our express application as a middleware
app.use(expressApp({ mysqlApp, appConfig, expressSession, cas }))

// Handle websocket connections
io.on('connection', socketioApp({ mysqlApp, appConfig, io, expressSession, cas }))

// Basically a while(true)
const port: number = serverConfig.get('port', 2880)
server.listen(port, () => {
  console.log('Listening to *:' + port)
})
