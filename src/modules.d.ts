declare module 'cas-authentication' {
  import type express from 'express'
  class CasAuthentication {
    constructor(options: any)
    bounce: express.RequestHandler
    bounce_redirect: express.RequestHandler
    logout: express.RequestHandler
    session_name: string
  }
  export = CasAuthentication
}

declare module 'express-session-rsdb' {
  class SessionRsdbStore {
    constructor(options: any)
  }
  export = SessionRsdbStore
}
