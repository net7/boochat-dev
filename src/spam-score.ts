export default function computeSpamScore (str: string) {
  let score = 0

  // Lengthy messages
  const tooLong = 100
  const scorePerExcedingCharacter = 2
  score += Math.max(0, str.length - tooLong) * scorePerExcedingCharacter

  // UPPER-CASE and punctuation
  const tooMuchUpperCase = Math.max(10, str.length / 3)
  const scorePerExcedingCapitalLetter = 1
  const upperLength = (str.match(/[^a-z]/g) || []).length
  score += Math.max(0, upperLength - tooMuchUpperCase) * scorePerExcedingCapitalLetter

  // Blacklisted words
  const blacklist = /anus|chatte|connard|pute|puta?in|salope/gi
  const scorePerBlacklistedWord = 10000
  score += (str.match(blacklist) || []).length * scorePerBlacklistedWord

  // URLs and phone numbers
  const scorePerURL = 100
  score += (str.match(/http:|[\w-.]+\.[a-z]{2,4}|(\d\d.?){5}/g) || []).length * scorePerURL

  if (score <= 0) return 0
  return Math.max(Math.log(score) / Math.log(10), 0)
}
