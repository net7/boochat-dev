---
git: net7/boochat-dev
---

# Boochat

Le boochat permet de te divertir lors des preshows interminables de CAn7 (ou le retard de TVn7).

![](./boochat-chat.png)

![](./boochat-game.png)
