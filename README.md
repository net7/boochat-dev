# Boochat

Un webchat pour les JTs.

## Comment lancer Boochat ?

* Installer [Node.js](https://nodejs.org/en/)
* Installer [Yarn](https://yarnpkg.com/)
* Installer les dépendances `yarn install`
* Lancer le service `yarn start`

## Technologies utilisées

Voici une liste non-exhaustive des technologies qui permettent de faire marcher Boochat.

* [Node.js](https://nodejs.org/en/) : interpréteur de JavaScript
* [Yarn](https://yarnpkg.com/) : gestionnaire de dépendances
* [Parcel](https://parceljs.org/) : compilateur pour le front-end
* [TypeScript](https://www.typescriptlang.org/) : JavaScript fortement typé
* [MariaDB](https://mariadb.com/) : serveur de base de données
* [Express](https://expressjs.com/) : bibliothèque pour créer un serveur HTTP
* [Socket.io](https://socket.io/) : bibliothèque pour créer un serveur WebSocket
* [Vue](https://vuejs.org/) : templating côté client
* [Stylus](https://stylus-lang.com/) : langage qui se compile en CSS

## Architecture


## Todos

* Commands : /kick, /ban, /pardon, /nick, /mod, /unmod
* slowdown -> animation
* CSS align-bottom

* autocompletion
* short commands
* /me.html
* permissions ??

Problems:
message shown state not updated
