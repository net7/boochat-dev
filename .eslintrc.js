module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "standard",
    'eslint:recommended',
    'plugin:vue/recommended'
  ],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
    parser: "@typescript-eslint/parser",
  },
  plugins: [
    "@typescript-eslint",
    "vue",
  ],
  rules: {
    "vue/html-self-closing": ["error", {
      "html": {
        "component": "any"
      },
    }]
  },
};
