// https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
const computeVh = () => {
  const vh = window.innerHeight * 0.01
  document.documentElement.style.setProperty('--vh', `${vh}px`)
}
computeVh()
window.addEventListener('resize', (e) => computeVh(), { passive: true })
