import io from 'socket.io-client'
// eslint-disable-next-line no-unused-vars
import Vue from 'vue'
import Mod from './mod.vue'
// eslint-disable-next-line no-unused-vars
import { DetailedMessage, Message, Notice } from '../../src/interfaces'

window.addEventListener('load', () => {
  const vm: Vue = new Mod({
    el: document.querySelector('#app')
  })

  const messageMap: Map<number, DetailedMessage> = new Map()
  const socket: SocketIOClient.Socket = io({ query: { mod: 1 } })
  let noticeCounter = 0
  let historyReceived = false

  // Events coming from the UI
  vm.$on('send-message', (body: string) => {
    socket.emit('message', body)
  })
  vm.$on('delete-message', (id: number) => {
    socket.emit('delete-message', id)
  })
  vm.$on('approve-message', (id: number) => {
    socket.emit('approve-message', id)
  })

  // Events coming from the server
  socket.on('connect', () => {
    vm.pushNotice({ id: ++noticeCounter, body: 'Connecté' })
  })
  socket.on('detailed-message', (detailedMessage: DetailedMessage) => {
    vm.pushDetailedMessage(detailedMessage)
    messageMap.set(detailedMessage.id, detailedMessage)
  })
  socket.on('detailed-history', (detailedMessages: DetailedMessage[]) => {
    if (!historyReceived) {
      historyReceived = true
      detailedMessages.forEach((detailedMessage) => {
        vm.pushDetailedMessage(detailedMessage)
        messageMap.set(detailedMessage.id, detailedMessage)
      })
    }
  })
  socket.on('notice', (body: string) => {
    vm.pushNotice({ id: ++noticeCounter, body })
  })
  socket.on('message', (message: Message) => {
    messageMap.get(message.id)?.validated = true
    messageMap.get(message.id)?.shown = true
  })
  socket.on('delete-message', (id: number) => {
    messageMap.get(id)?.validated = false
    messageMap.get(id)?.shown = false
  })
  socket.on('team-update', (teams: Team[]) => {
    vm.updateTeams(teams)
  })
  socket.on('error', (error) => {
    if (!error?.code) {
      error = { code: 'E_GENERIC', message: error.message }
      if (error.message === 'websocket error') {
        error.code = 'E_CONNECTION_LOST'
        error.message = 'Connexion perdue'
      }
    }
    console.error(error)
    vm.pushNotice({ id: ++noticeCounter, body: error.message })
  })
}, {})
