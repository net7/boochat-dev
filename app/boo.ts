import io from 'socket.io-client'
// eslint-disable-next-line no-unused-vars
import Vue from 'vue'
import BooMessageList from './boo-message-list.vue'
// eslint-disable-next-line no-unused-vars
import { Message, Team } from '../src/interfaces'

window.addEventListener('load', () => {
  const vm: Vue = new BooMessageList({
    el: document.querySelector('#app')
  })

  const socket: SocketIOClient.Socket = io()
  let historyReceived = false

  socket.on('message', (message: Message) => {
    vm.pushMessage(message)
  })
  socket.on('history', (messages: Message[]) => {
    if (!historyReceived) {
      historyReceived = true
      messages.forEach((message) => vm.pushMessage(message))
    }
  })
  socket.on('delete-message', (id: number) => {
    vm.deleteMessage(id)
  })
  socket.on('team-update', (teams: Team[]) => {
    vm.updateTeams(teams)
  })
  socket.on('error', (error) => {
    if (!error?.code) {
      error = { code: 'E_GENERIC', message: error.message }
      if (error.message === 'websocket error' || error.message === 'xhr poll error') {
        error.code = 'E_CONNECTION_LOST'
        error.message = 'Connexion perdue'
      }
    }
    console.error(error)
  })
}, {})
