import io from 'socket.io-client'
// eslint-disable-next-line no-unused-vars
import Vue from 'vue'
import App from './app.vue'
// eslint-disable-next-line no-unused-vars
import { DetailedMessage, Message, Team } from '../src/interfaces'

window.addEventListener('load', () => {
  const vm: Vue = new App({
    el: document.querySelector('#app')
  })

  const socket: SocketIOClient.Socket = io()
  let noticeCounter = 0
  let historyReceived = false

  vm.$on('send-message', (body: string) => {
    socket.emit('message', body)
  })
  socket.on('connect', () => {
    // Great news, we're connected
  })
  socket.on('notice', (body: string) => {
    vm.pushNotice({ id: ++noticeCounter, body })
  })
  socket.on('message', (message: Message) => {
    vm.pushMessage(message)
  })
  socket.on('history', (messages: Message[]) => {
    if (!historyReceived) {
      historyReceived = true
      messages.forEach((message) => vm.pushMessage(message))
    }
  })
  socket.on('delete-message', (id: number) => {
    vm.deleteMessage(id)
  })
  socket.on('team-update', (teams: Team[]) => {
    vm.updateTeams(teams)
  })
  socket.on('chat-timings', ({
    moderationDelay,
    slowdown
  }: {
    moderationDelay: number,
    slowdown: number
  }) => {
    vm.chatTimings({ moderationDelay, slowdown })
  })
  socket.on('error', (error) => {
    if (!error?.code) {
      error = { code: 'E_GENERIC', message: error.message }
      if (error.message === 'websocket error' || error.message === 'xhr poll error') {
        error.code = 'E_CONNECTION_LOST'
        error.message = 'Connexion perdue'
      }
    }
    console.error(error)
    if (error.code === 'E_CONNECTION_LOST') {
      setTimeout(() => {
        if (!socket.connected) {
          vm.pushNotice({ id: ++noticeCounter, body: 'Connexion perdue, reconnexion en cours...' })
        }
      }, 5000)
    } else {
      vm.pushNotice({ id: ++noticeCounter, body: error.message })
    }
  })
}, {})
